define({
  files: {
    'no files': 'No files'
  },
  login_form: {
    'hello': 'Hello',
    'logout': 'Logout',
    'sign up': 'Sign up',
    'login': 'Login',
    'email': 'Email',
    'username': 'Name',
    'password': 'Password'
  },
  global: {
    'error': 'error',
    'warning': 'warning',
    'cleanComplete': 'All expressions are cleared.'
  },
  plots: {
    'Download': 'Download',
    'DownloadTooltip': 'Download image',
    'Plot': 'Plot',
    'PlotTooltip': 'Plot graphics with current settings',
    'Cut line': 'Cut line',
    'Set new line': 'Set new line',
    'Del old line': 'Delete old line',
    'Del new line': 'Delete new line',
    'isEqualScaleTooltip': 'Equal scale for X and Y axes (press "Enter" or click "Plot" to apply changes)',
    'isEqualScale': 'Equal scale',
    'isBlackWhiteTooltip': 'Black and white graphics (press "Enter" or click "Plot" to apply changes)',
    'isBlackWhite': 'B\\w',
    'font-size': 'Font size (press "Enter" or click "Plot" to apply changes)',
    'line thickness': 'Line thickness (press "Enter" or click "Plot" to apply changes)',
    'axes thickness': 'Axes thickness (press "Enter" or click "Plot" to apply changes)',
    'number of frames': 'Number of frames (press "Enter" or click "Plot" to apply changes)',
    '#frames': '#frames'
  },
  student: {
    'task title placeholder': 'Test title',
    'save notebook to db': 'Save text as new Test',
    'task id placeholder': 'Test ID',
    'load task by id': 'Load test by ID',
    'show edu plan': 'Plan',
    'show record book': 'Grade book',
    'check': 'Check',
    'check and run': 'Run+Ch',
    'give up': 'Give up'
  },
  eduplan: {
    'id': 'ID',
    'task title': 'Test title',
    'delete task': 'Delete'
  }
});
