define({
  files: {
    'no files': 'Нет файлов'
  },
  login_form: {
    'hello': 'Добро пожаловать',
    'logout': 'Выйти',
    'sign up': 'Регистрация',
    'login': 'Войти',
    'email': 'Email',
    'username': 'Имя',
    'password': 'Пароль'
  },
  global: {
    'error': 'Ошибка',
    'warning': 'Предупреждение',
    'cleanComplete': 'Все выражения очищены.'
  },
  plots: {
    'Download': 'Скачать',
    'DownloadTooltip': 'Скачать изображение',
    'Plot': 'Построить',
    'PlotTooltip': 'Построить график с текущими настройками',
    'Cut line': 'Вырезать линию',
    'Set new line': 'Добавить линию',
    'Del old line': 'Удалить старую линию',
    'Del new line': 'Удалить новую линию',
    'isEqualScaleTooltip': 'Использовать одинаковый масштаб по осям X и Y',
    'isEqualScale': 'Одинаковый масштаб',
    'isBlackWhiteTooltip': 'Черно-белый режим (нажмите "Enter" или кликните "Plot", чтобы применить изменения)',
    'isBlackWhite': 'Ч\\б',
    'font-size': 'Размер шрифта (нажмите "Enter" или кликните "Plot", чтобы применить изменения)',
    'line thickness': 'Толщина линии (нажмите "Enter" или кликните "Plot", чтобы применить изменения)',
    'axes thickness': 'Толщина осей (нажмите "Enter" или кликните "Plot", чтобы применить изменения)',
    'number of frames': 'Количество кадров (нажмите "Enter" или кликните "Plot", чтобы применить изменения)',
    '#frames': 'Кадров'
  },
  student: {
    'task name placeholder': 'Название теста',
    'save notebook to db': 'Сохранить как текст',
    'task id placeholder': 'ID теста',
    'load task by id': 'Загрузить теста по ID',
    'show edu plan': 'Учебный план',
    'show record book': 'Зачетная книжка',
    'check': 'Проверить',
    'give up': 'Показать решение'
  },
  eduplan: {
    'id': 'ID',
    'task title': 'Название теста',
    'delete task': 'Удалить'
  }
});
