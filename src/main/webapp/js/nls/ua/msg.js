define({
  files: {
    'no files': 'Немає файлів'
  },
  login_form: {
    'hello': 'Ласкаво просимо',
    'logout': 'Вийти',
    'sign up': 'Реєстрація',
    'login': 'Увійти',
    'email': 'Email',
    'username': "Ім'я",
    'password': 'Пароль'
  },
  global: {
    'error': 'Помилка',
    'warning': 'Попередження',
    'cleanComplete': 'Усі вирази очищені.'
  },
  plots: {
    'Download': 'Завантажити',
    'DownloadTooltip': 'Завантажити зображення',
    'Plot': 'Построить',
    'PlotTooltip': 'Побудувати графік із поточними налаштуваннями',
    'Cut line': 'Вирізати лінію',
    'Set new line': 'Установити нову лінію',
    'Del old line': 'Видалити стару лінію',
    'Del new line': 'Видалити нову лінію',
    'isEqualScaleTooltip': 'Використовувати однаковий масштаб по осях X та Y',
    'isEqualScale': 'Однаковий масштаб',
    'isBlackWhiteTooltip': 'Чорно-білий режим (натисніть "Enter" або клацніть "Plot", щоб застосувати зміни)',
    'isBlackWhite': 'Ч\\б',
    'font-size': 'Розмір шрифта (натисніть "Enter" або клацніть "Plot", щоб застосувати зміни)',
    'line thickness': 'Товщина ліній (натисніть "Enter" або клацніть "Plot", щоб застосувати зміни)',
    'axes thickness': 'Товщина осей (натисніть "Enter" або клацніть "Plot", щоб застосувати зміни)',
    'number of frames': 'Кількість кадрів (натисніть "Enter" або клацніть "Plot", щоб застосувати зміни)',
    '#frames': 'Кадри'
  },
  student: {
    'task name placeholder': 'Назва тесту',
    'save notebook to db': 'Зберегти як текст ',
    'task id placeholder': 'ID теста',
    'load task by id': 'Завантажити тест по ID',
    'show edu plan': 'Навчальний план',
    'show record book': 'Залікова книжка',
    'check': 'Перевірити',
    'give up': "Показати розв'язок"
  },
  eduplan: {
    'id': 'ID',
    'task title': 'Назва теста',
    'delete task': 'Видалити'
  }
});
