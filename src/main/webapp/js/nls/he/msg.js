﻿define({
  files: {
    'no files': 'אין קבצים'
  },
  login_form: {
    'hello': 'שלום',
    'logout': 'התנתק',
    'sign up': 'הירשם',
    'login': 'התחבר',
    'email': 'כתובת דוא"ל',
    'username': 'שם משתמש',
    'password': 'סיסמה'
  },
  global: {
    'error': 'שגיאה',
    'warning': 'אזהרה',
    'cleanComplete': 'כל הביטויים נמחקו'
  },
  plots: {
    'Download': 'הורדה',
    'DownloadTooltip': 'הורד את התמונה',
    'Plot': 'גרף',
    'PlotTooltip': 'יצא פלט גרפי עם ההגדרות הנוכחיות',
    'Cut line': 'חתוך את השורה',
    'Set new line': 'הגדר שורה חדשה',
    'Del old line': 'מחק שורה ישנה',
    'Del new line': 'מחק שורה חדשה',
    'isEqualScaleTooltip': 'כפה קנה מידה שווה עבור ציר הX וציר הY',
    'isEqualScale': 'בקנה מידה שווה',
    'isBlackWhiteTooltip': 'השתמש רק בשחור-לבן עבור גרפיקה',
    'isBlackWhite': 'שחור/לבן',
    'font-size': 'גודל גופן',
    'line thickness': 'עובי הקו',
    'axes thickness': 'עובי הצירים',
    'number of frames': 'מספר המסגרות',
    '#frames': '#frames'
  },
  student: {
    'task title placeholder': 'בדוק את הכותרת',
    'save notebook to db': 'שמור טקסט כמשימה חדשה',
    'task id placeholder': 'מזהה משימה',
    'load task by id': 'טען משימה לפי מזהה',
    'show edu plan': 'תכנית',
    'show record book': 'רשומות ציונים',
    'check': 'בדיקה',
    'check and run': 'הרץ ובדוק',
    'give up': 'וויתור'
  },
  eduplan: {
    'id': 'ID',
    'task title': 'כותרת משימה',
    'delete task': 'מחק משימה'
  }
});
