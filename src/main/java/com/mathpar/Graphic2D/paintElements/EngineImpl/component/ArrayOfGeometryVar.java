package com.mathpar.Graphic2D.paintElements.EngineImpl.component;

import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import lombok.Getter;
import lombok.Setter;

public class ArrayOfGeometryVar implements GeometryVar {



    public ArrayOfGeometryVar(GeometryVar[] vars) {
        this.vars = vars;
        if(vars != null)
            this.N = vars.length;
    }

    @Getter
    @Setter
    GeometryVar[] vars;
    @Getter
    @Setter
    int N;



    @Override
    public String draw(){
        StringBuilder res = new StringBuilder();
        for(GeometryVar p : vars)
            res.append(p.draw()).append("\n");
        return res.toString();
    }

    @Override
    public ArgType getType() {
        return ArgType.ARRAY_OF_GEOMETRYVARS;
    }


}
