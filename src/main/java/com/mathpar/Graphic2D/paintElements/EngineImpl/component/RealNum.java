package com.mathpar.Graphic2D.paintElements.EngineImpl.component;

import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import lombok.*;

@Data
@RequiredArgsConstructor
public class RealNum implements GeometryVar {

    private @NonNull double value;

    @Override
    public ArgType getType() {
        return ArgType.REAL_NUM;
    }

    @Override
    public String draw() {
        return "";
    }

    @Override
    public boolean canCast(ArgType argType) {
        return argType == getType() || (
                argType == ArgType.INT && Math.floor(value) == value && Double.isFinite(value)
        );
    }

    @Override
    public GeometryVar castTo(ArgType argType) {
        if (argType == getType()) {
            return this;
        }

        if (argType == ArgType.INT) {
            return new IntNum((int) value);
        }

        return null;
    }
}
