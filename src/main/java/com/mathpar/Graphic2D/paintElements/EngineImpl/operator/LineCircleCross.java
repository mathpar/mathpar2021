package com.mathpar.Graphic2D.paintElements.EngineImpl.operator;

import com.mathpar.Graphic2D.paintElements.EngineImpl.component.ArrayOfGeometryVar;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Circle;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Line;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Point;
import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.OperatorException;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.Operator;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.model.ScreenParams;
import com.mathpar.number.Ring;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class LineCircleCross implements Operator {

    private static final List<ArgType> argTypes = Collections.unmodifiableList(Arrays.asList(
            ArgType.LINE, ArgType.CIRCLE
    ));

    @Override
    public List<ArgType> getArgTypes() {
        return argTypes;
    }

    @Override
    public String getFullName() {
        return "lineCircleCross";
    }

    //TODO
    @Override
    public GeometryVar apply(List<GeometryVar> args, ScreenParams screen, Ring ring) {

        ListIterator<GeometryVar> argsIt = args.listIterator();
        Line start_line = (Line) argsIt.next();
        Circle circle = (Circle) argsIt.next();

        double delta_x = circle.getCenter().getX();
        double delta_Y = circle.getCenter().getY();
        circle = new Circle(circle.getRadius());
        Line line = new Line(new Point(start_line.point1.x-delta_x, start_line.point1.y - delta_Y), new Point(start_line.point2.x-delta_x, start_line.point2.y - delta_Y) );

        double A = 1;
        double B = -1/line.getK();
        double C = - line.getB();
        double v = Math.pow(A, 2) + Math.pow(B, 2);
        double x0 = -(A*C)/ v;
        double y0 = -(B*C)/ v;
        double d = Math.pow(circle.getRadius(),2) - Math.pow(C,2)/v;
        double mult = Math.sqrt(d/v);
        List<Point> points = Collections.unmodifiableList(Arrays.asList(
                new Point(x0+B*mult + delta_x, y0 - A* mult + delta_Y), new Point(x0 - B*mult + delta_x, y0 + A* mult + delta_Y)
        ));
        points = points.stream().filter(start_line::containPoint).collect(Collectors.toList());

        return new ArrayOfGeometryVar((Point[]) points.toArray());
    }
}
