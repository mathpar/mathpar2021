package com.mathpar.Graphic2D.paintElements.EngineImpl.register;

public enum ArgType {
    ARRAY_OF_GEOMETRYVARS,
    REAL_NUM,
    CIRCLE,
    LINE,
    POINT,
    TRIANGLE,
    VOID,
    REGULAR_POLYGON,
    INT,
    STRING,
    TEXT,
    ELLIPSE,
}
