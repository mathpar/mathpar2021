package com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces;

import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Point;
import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;

public interface GeometryVar {

    // TODO: implement variables of different types

    default Point getDisplayPoint() {
        return null;
    }

    ArgType getType();

    String draw();

    default boolean canCast(ArgType argType){
        return argType == getType();
    }

    default GeometryVar castTo(ArgType argType){
        if (canCast(argType)) {
            return this;
        }

        return null;
    }
}
