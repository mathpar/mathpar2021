package com.mathpar.Graphic2D.paintElements.GeometryEngine.error;

public class EngineException extends Exception {
    private static final long serialVersionUID = -5365630128856068164L + 235L;

    public EngineException(String s) {
        super(s);
    }

    public EngineException(String message, Throwable cause) {
        super(message, cause);
    }

    public EngineException(Throwable cause) {
        super(cause);
    }
}
