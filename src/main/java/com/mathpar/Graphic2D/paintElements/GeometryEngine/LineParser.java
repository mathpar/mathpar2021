package com.mathpar.Graphic2D.paintElements.GeometryEngine;

import com.mathpar.Graphic2D.paintElements.EngineImpl.component.IntNum;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.RealNum;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.StringValue;
import com.mathpar.Graphic2D.paintElements.EngineImpl.register.ArgType;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.EngineException;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.ParseException;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.Operator;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.VarStorage;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.model.ParserParams;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.model.VarMap;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.model.VarModel;
import lombok.AllArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

public class LineParser {

    private static final String DISPLAY_OP_TOKEN = "display";
    private static final VarStorage EMPTY_STORAGE = new VarMap();
    private static final char STRING_DELIM = '%';

    public static VarModel parse(ParserParams parserParams) throws EngineException {
        Token token = extractToken(parserParams.getLine(), 0);

        if (token.kind == TokenKind.POSSIBLE_RAW) {
            throw ParseException.at(0, "variable or operator", token.value);
        }

        if (token.kind == TokenKind.OPERATOR) {
            validateOperator(token, parserParams.getOperators());
            return parseOperator(token, parserParams);
        }

        return parseNewVar(token, parserParams);
    }

    public static String argsCanonForm(List<ArgType> argTypes) {
        return argTypes.toString();
    }

    private static VarModel parseNewVar(Token leftToken, ParserParams parserParams) throws EngineException {
        String line = parserParams.getLine();

        validateNewVar(leftToken, parserParams.getVars());

        if (line.length() <= leftToken.location) {
            throw ParseException.at(leftToken.location, "=", "end of line");
        }
        if (line.charAt(leftToken.location) != '=') {
            throw ParseException.at(leftToken.location, "=", line.charAt(leftToken.location));
        }

        Token rightToken = extractToken(line, leftToken.location + 1);
        resolveTokenKind(rightToken, parserParams.getVars(), parserParams.getOperators());

        VarModel varModel = convertToVarModel(rightToken, parserParams);
        varModel.setName(leftToken.value);

        parserParams.getVars().put(varModel.getName(), varModel.getVar());

        return varModel;
    }

    private static Token extractToken(String line, int location) {
        TokenKind tokenKind = TokenKind.POSSIBLE_RAW;
        StringBuilder value = new StringBuilder();
        char ch;
        boolean isString = false;

        loop: for (; location < line.length(); location++) {
            ch = line.charAt(location);

            // TODO: add infix by extracting till the end of a whitespace trail

            if (ch == STRING_DELIM) {
                isString = !isString;
            }

            if (!isString) {
                switch (ch) {
                    case '=':
                        tokenKind = TokenKind.VAR;
                        break loop;
                    case '(':
                        tokenKind = TokenKind.OPERATOR;
                        break loop;
                    case ',':
                    case ')':
                        break loop;
                }
            }

            value.append(ch);
        }

        return new Token(tokenKind, value.toString(), location);
    }

    private static void validateNewVar(Token token, VarStorage vars) throws ParseException {
        String value = token.value;
        int originalLength = value.length();

        value = value.trim();
        token.value = value;
        int length = value.length();

        if (length == 0) {
            throw ParseException.at(token.location + originalLength, "expected variable name");
        }

        if (vars.contains(value)) {
            return;
        }

        if (!value.chars().allMatch(Character::isLetterOrDigit)) {
            throw ParseException.at(token.location, "variable name must be alphanumeric");
        }

        if (!Character.isLetter(value.charAt(0))) {
            throw ParseException.at(token.location, "variable name must start with a letter");
        }
    }

    private static void validateOperator(Token token,
                                         Map<String, Map<String, Operator>> operators) throws ParseException {
        token.value = token.value.trim();
        if (operators.containsKey(token.value)) {
            return;
        }

        throw ParseException.at(token.location, "Unknown operator: " + token.value);
    }

    private static void resolveTokenKind(Token token,
                                         VarStorage vars,
                                         Map<String, Map<String, Operator>> operators) throws ParseException {
        // TODO: add more robust RawValue system based on GeometryVar

        // validate that there are no assignments in operator params
        if (token.kind == TokenKind.VAR) {
            throw ParseException.at(token.location, "raw value, var name or operator", "=");
        }

        String value = token.value;
        value = value.trim();
        token.value = value;

        if (token.kind == TokenKind.OPERATOR) {
            validateOperator(token, operators);
            return;
        }

        if (vars.contains(value)) {
            token.kind = TokenKind.VAR;
            return;
        }

        if (value.charAt(0) == value.charAt(value.length() - 1) && value.charAt(0) == STRING_DELIM) {
            token.value = token.value.substring(1, value.length() - 1);
            token.kind = TokenKind.RAW_STRING;
            return;
        }

        try {
            Double.parseDouble(value);
            token.kind = TokenKind.RAW_DOUBLE;
            return;
        } catch (Throwable ignored) {
        }

        throw ParseException.at(token.location, "invalid value");
    }

    private static VarModel parseOperator(Token operatorToken, ParserParams parserParams) throws EngineException {
        Map<String, Map<String, Operator>> operators = parserParams.getOperators();
        String line = parserParams.getLine();
        VarStorage vars = parserParams.getVars();

        // extract operator arguments
        Token argToken;
        VarModel varModel;
        int location = operatorToken.location + 1;
        List<GeometryVar> args = new ArrayList<>();

        for (; location < line.length(); location++) {
            argToken = extractToken(line, location);
            resolveTokenKind(argToken, vars, operators);
            varModel = convertToVarModel(argToken, parserParams);

            args.add(varModel.getVar());
            location = varModel.getLocation();

            if (line.charAt(location) == ')') {
                break;
            }
        }

        if (line.length() <= location) {
            throw ParseException.at(location, ")", "end of line");
        }

        // collect operator by name and arguments
        args = Collections.unmodifiableList(args);

        String typeString = argsCanonForm(args
                .stream()
                .map(GeometryVar::getType)
                .collect(Collectors.toList())
        );

        Map<String, Operator> operator2args = operators.get(operatorToken.value);

        Operator operator = operator2args.get(typeString);
        if (operator == null) {
            OperatorWithArgs operatorWithArgs = castArgs(operator2args, args);
            operator = operatorWithArgs.operator;
            args = operatorWithArgs.args;
        }
        if (operator == null) {
            throw ParseException.at(operatorToken.location, String.format(
                    "no operator '%s' exists with args '%s'",
                    operatorToken.value, typeString
            ));
        }

        // apply operator
        GeometryVar result = operator.apply(args, parserParams.getScreen(), parserParams.getRing());

        // extract label
        VarModel varWithLabel = extractLabel(line, location);
        varWithLabel.setVar(result);
        varWithLabel.setProcessedInput(line);

        return varWithLabel;
    }

    private static OperatorWithArgs castArgs(Map<String, Operator> operators, List<GeometryVar> args) {
        Operator result = null;
        List<GeometryVar> newArgs = null;

        int i;
        GeometryVar arg;
        ArgType argType;
        List<ArgType> argTypes;

        for (Operator operator : operators.values()) {
            argTypes = operator.getArgTypes();

            if (argTypes.size() != args.size()) {
                continue;
            }

            newArgs = new ArrayList<>();

            for (i = 0; i < argTypes.size(); i++) {
                argType = argTypes.get(i);
                arg = args.get(i);

                if (arg.getType() == argType) {
                    newArgs.add(arg);
                    continue;
                }

                if (!arg.canCast(argType)) {
                    break;
                }

                newArgs.add(arg.castTo(argType));
            }

            if (i == argTypes.size()) {
                result = operator;
                break;
            }
        }

        return new OperatorWithArgs(result, newArgs);
    }

    private static VarModel extractLabel(String line, int location) throws ParseException {
        String label = null;

        extraction:
        {
            location++;

            if (line.length() <= location) {
                break extraction;
            }

            if (line.charAt(location) != '.') {
                break extraction;
            }

            location++;
            Token displayOpToken = extractToken(line, location);
            if (displayOpToken.kind != TokenKind.OPERATOR) {
                throw ParseException.at(location, "expected operator (e.g. \"" + DISPLAY_OP_TOKEN + "\"");
            }

            location = displayOpToken.location + 1;
            Token labelToken = extractToken(line, location);
            if (labelToken.value.length() == 0) {
                label = "";
                break extraction;
            }

            resolveTokenKind(labelToken, EMPTY_STORAGE, Collections.emptyMap());

            if (labelToken.kind != TokenKind.RAW_STRING) {
                throw ParseException.at(location, "display name (of type String)", labelToken.value);
            }

            if (line.length() <= labelToken.location) {
                throw ParseException.at(labelToken.location, ")", "end of line");
            }

            if (line.charAt(labelToken.location) != ')') {
                throw ParseException.at(labelToken.location, ")", line.charAt(labelToken.location));
            }

            label = labelToken.value;
            location = labelToken.location;
        }

        return new VarModel(null, label, null, null, location);
    }

    private static VarModel convertToVarModel(Token argToken, ParserParams parserParams) throws EngineException {
        if (argToken.kind == TokenKind.OPERATOR) {
            return parseOperator(argToken, parserParams);
        }

        VarModel varModel = new VarModel(null, null, null, null, argToken.location);

        switch (argToken.kind) {
            case VAR:
                varModel.setVar(
                        parserParams.getVars().get(argToken.value)
                );
                break;

            case RAW_STRING:
                varModel.setVar(
                        new StringValue(argToken.value)
                );
                break;

            case RAW_INT:
                varModel.setVar(
                        new IntNum(Integer.parseInt(argToken.value))
                );
                break;

            case RAW_DOUBLE:
                varModel.setVar(
                        new RealNum(Double.parseDouble(argToken.value))
                );
                break;

            default:
                throw ParseException.at(argToken.location, "invalid value");
        }

        return varModel;
    }

    @AllArgsConstructor
    private static class OperatorWithArgs {
        Operator operator;
        List<GeometryVar> args;
    }

    @AllArgsConstructor
    private static class Token {
        TokenKind kind;
        String value;
        int location;
    }

    private enum TokenKind {
        VAR,
        OPERATOR,
        POSSIBLE_RAW,
        RAW_STRING,
        RAW_INT,
        RAW_DOUBLE,
    }

}
