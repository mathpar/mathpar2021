package com.mathpar.Graphic2D.paintElements.GeometryEngine.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FirstLineOutput {
    int startIndex;
    ScreenParams screen;
}
