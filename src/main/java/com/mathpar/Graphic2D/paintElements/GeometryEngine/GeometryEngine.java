package com.mathpar.Graphic2D.paintElements.GeometryEngine;

import com.mathpar.Graphic2D.paintElements.EngineImpl.component.IntNum;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Point;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.StringValue;
import com.mathpar.Graphic2D.paintElements.EngineImpl.component.Text;
import com.mathpar.Graphic2D.paintElements.EngineImpl.operator.TextByStartPointAndFontSize;
import com.mathpar.Graphic2D.paintElements.EngineImpl.register.OperatorsRegister;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.EngineException;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.OperatorException;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.error.ParseException;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.GeometryVar;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.Operator;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.interfaces.VarStorage;
import com.mathpar.Graphic2D.paintElements.GeometryEngine.model.*;
import com.mathpar.func.Page;
import com.mathpar.number.Ring;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class GeometryEngine {

    private static final Map<String, Map<String, Operator>> OPERATORS = Collections.unmodifiableMap(
            registerOperators()
    );
    private static final String DELIMITER = ";";

    public static GeometryEngineOutput parse(String input, Ring ring, Page page) {
        VarStorage vars = new VarMap();
        Map<String, String> labels = new HashMap<>();

        List<String> lines = Arrays
                .stream(input.split(DELIMITER))
                .map(String::trim)
                .filter(l -> !l.isEmpty())
                .collect(Collectors.toList());
        int ww=2;
        int yy=ww+1;
        if (lines.isEmpty()) {
            return new GeometryEngineOutput("", "", null);
        }

        ScreenParams screen = new ScreenParams(page.xMin, page.xMax, page.yMin, page.yMax);
        ParserParams parserParams = new ParserParams(null, vars, OPERATORS, screen, ring);

        EngineException error;
        StringBuilder processedInput = new StringBuilder();
        VarModel varModel;

        for (int i = 0; i < lines.size(); i++) {
            parserParams.setLine(lines.get(i));

            try {
                varModel = LineParser.parse(parserParams);
            } catch (ParseException e) {
                error = new ParseException(String.format(
                        "Parse exception at line %s, %s",
                        i, e.getMessage()
                ), e);
                return new GeometryEngineOutput(null, null, error);
            } catch (EngineException e) {
                error = new EngineException(String.format(
                        "Parse exception at line %s: %s",
                        i, e.getMessage()
                ), e);
                return new GeometryEngineOutput(null, null, error);
            }

            processedInput.append(varModel.getProcessedInput());
            processedInput.append(DELIMITER);
            processedInput.append('\n');

            if (varModel.getName() != null && varModel.getLabel() != null) {
                labels.put(varModel.getName(), varModel.getLabel());
            }
        }

        Set<String> labelKeySet = labels.keySet();
        if (labelKeySet.size() == 0) {
            return new GeometryEngineOutput(processedInput.toString(), "", null);
        }

        String plots = labelKeySet
                .stream()
                .filter(Objects::nonNull)
                .map(varName -> draw(varName, labels.get(varName), vars.get(varName), screen, ring))
                .collect(Collectors.joining());

        String keys = String.join(", ", labelKeySet);
        String labelKeys = labelKeySet
                .stream()
                .filter(Objects::nonNull)
                .map(GeometryEngine::labelName)
                .collect(Collectors.joining(", "));

        String outputCode = String.format("%s\\showPlots([%s, %s]);\n", plots, keys, labelKeys);

        return new GeometryEngineOutput(processedInput.toString(), outputCode, null);
    }

    private static String draw(String varName, String label, GeometryVar var, ScreenParams screen, Ring ring) {
        Point displayPoint = var.getDisplayPoint();

        if (displayPoint == null) {
            displayPoint = new Point(0, 0);
            label = "";
        }

        if (var instanceof Text) {
            label = "";
        }

        String drawVar = varName + " = " + var.draw() + DELIMITER + "\n";
        String drawLabel = labelName(varName) + " = ";

        try {
            drawLabel += new TextByStartPointAndFontSize()
                    .apply(
                            Arrays.asList(
                                    new StringValue(label), displayPoint, new IntNum(20)
                            ),
                            screen, ring
                    ).draw();
        } catch (OperatorException ignored) {
        }

        return drawVar + drawLabel + DELIMITER + "\n";
    }

    private static String labelName(String varName) {
        return varName + "Label";
    }

    private static Map<String, Map<String, Operator>> registerOperators() {
        Map<String, Map<String, Operator>> operators = new HashMap<>();

        String operatorName;
        Map<String, Operator> types;
        String typeString;
        Operator presentOperator;

        for (Operator operator : OperatorsRegister.OPERATORS) {
            operatorName = operator.getFullName();
            types = operators.computeIfAbsent(operatorName, k -> new HashMap<>());

            typeString = LineParser.argsCanonForm(operator.getArgTypes());
            presentOperator = types.get(typeString);

            if (presentOperator != null) {
                log.error(String.format(
                        "Operators clash: '%s' and '%s' have the same definition",
                        presentOperator.getClass().getCanonicalName(),
                        operator.getClass().getCanonicalName()
                ));
                continue;
            }

            types.put(typeString, operator);
        }

        return operators;
    }

    public static void main(String[] args) {
        String input = "set2D(-10, 10, -10, 10);\n" +
                "a = Point(0, 8).display(%A%);\n" +
                "b = Point(0, 0).display(%B%);\n" +
                "c = Point(8, 0).display(%C%);\n" +
                "l2 = Line(Point(2, 2), Point(4, 4)).display(%L2%);\n" +
                "t = Triangle(a, b, c).display(%T%);\n" +
                "i = incircle(t).display(%I%);";
        GeometryEngineOutput engineOutput = parse(input, null, null);
        System.out.println("processedInput: \n\n" + engineOutput.getProcessedInput());
        System.out.println("outputCode: \n\n" + engineOutput.getOutputCode());


        EngineException error = engineOutput.getError();
        if (error != null) {
            error.printStackTrace();
        }

    }
}
