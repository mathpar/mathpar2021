<!-- footer.ftl -->
 <script>
          // $("form").wrap("<div id = 'collapseGraph' class='hidden'> </div>").parent().html();
        $(document).ready(function(){
          $("#bGraph").click(function(){
            $(".form-inline").slideToggle("slow");
          });
        });

        </script>
  <!--hide sidebar for mobile devices-->
  <script>

if ($(window).width() < 768) {
        // do something for small screens
        $('.panel-collapse').addClass('collapse');
        $('#txt').attr("rows", "15");
    }
    else if ($(window).width() >= 768 &&  $(window).width() <= 992) {
        // do something for medium screens
        $('.panel-collapse').removeClass('collapse');
        $('#txt').attr("rows", "5");
    }
    else if ($(window).width() > 992 &&  $(window).width() <= 1200) {
        // do something for big screens
        $('.panel-collapse').removeClass('collapse');
        $('#txt').attr("rows", "1");
    }
    else  {
        // do something for huge screens
        $('.panel-collapse').removeClass('collapse');
        $('#txt').attr("rows", "1");
    }

</script>
</body>
</html>
