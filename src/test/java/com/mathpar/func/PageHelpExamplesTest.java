/**
* Copyright © 2011 Mathparca Ltd. All rights reserved.
*/

package com.mathpar.func;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    PageHelp002IntroTest.class,
    PageHelp003PlotsTest.class,
    PageHelp004ParadigmTest.class,
    PageHelp005FunctionsTest.class,
    PageHelp006SeriesTest.class,
    PageHelp011ScriptingTest.class
})
public class PageHelpExamplesTest {
}
