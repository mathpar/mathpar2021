FROM tomcat:9-jdk8-openjdk

RUN rm -r webapps/* | true
ADD target/mathpar.war /usr/local/tomcat/webapps/ROOT.war